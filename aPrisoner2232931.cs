namespace PrisonerDilemma;
class aPrisoner2232931 : IPrisoner
{
  public string Name { get; } = "Dmitriy";
  private PrisonerChoice opponent { get; set; } = PrisonerChoice.Defect;
  private int counter { get; set; } = 0;

  public void Reset(DilemmaParameters dilemmaParameters) { 
    this.opponent = PrisonerChoice.Defect;
    this.counter = 0;
  }

  public PrisonerChoice GetChoice()
  {
    if (counter == 0) {
        this.counter++;
        return PrisonerChoice.Defect;
    }
    else if (opponent == PrisonerChoice.Cooperate) {
        return PrisonerChoice.Cooperate;
    }
    else {
        return PrisonerChoice.Defect;
    }
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) { 
    this.opponent =  otherChoice;
  }
}