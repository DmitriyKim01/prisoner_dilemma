using System.Collections.Immutable;

// TODO : How is this namespace directive different than the one with `{}`?
namespace PrisonerDilemma;

/// <summary>
/// Round-robin tournament of iterated prisoner's dilemmas.
/// </summary>
class PrisonerTournament
{
  // TODO : What is the advantage of adding `init` here?
  /// <summary>
  /// Prisoners participating in the tournament.
  /// </summary>
  public ImmutableArray<IPrisoner> Prisoners { get; init; }

  // TODO : What access modifier is used when none is explicitely written?
  IteratedDilemma IteratedDilemma { get; }

  public PrisonerTournament(
    ImmutableArray<IPrisoner> prisoners, int numIterations
    )
  {
    Prisoners = prisoners;
    IteratedDilemma = new IteratedDilemma(numIterations);
  }

  /// <summary>
  /// Starts the tournament, simulating iterated prisoner's dilemmas between
  /// every distinct pair (order is ignored i.e. (a,b) == (b,a))) of prisoners.
  /// </summary>
  public double[] Start()
  {
    double[] scores = new double[Prisoners.Length];

    // TODO : What does the Select method allow us to do here?
    foreach ((IPrisoner firstPrisoner, int i) in
      Prisoners.Select((prisoner, index) => (prisoner, index)))
    {
      foreach ((IPrisoner secondPrisoner, int j) in
        Prisoners.Select((prisoner, index) => (prisoner, index)))
      {
        if (i <= j)
        {
          // Skipping the simulation between a prisoner and themself,
          // as well as simulations that already occured.
          continue;
        }

        firstPrisoner.Reset(IteratedDilemma.Parameters);
        secondPrisoner.Reset(IteratedDilemma.Parameters);

        (double firstPrisonerScore, double secondPrisonerScore) =
          IteratedDilemma.Simulate(
            firstPrisoner, secondPrisoner
            );

        scores[i] += firstPrisonerScore;
        scores[j] += secondPrisonerScore;
      }

      scores[i] /= Prisoners.Length - 1;
    }

    return scores;
  }
}